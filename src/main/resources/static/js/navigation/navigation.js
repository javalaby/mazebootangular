angular.module('navigation',[]).component('navigation', {
	templateUrl: 'js/navigation/login.html',
	
	controller : function(auth) {
		
		var self = this;

		self.credentials = {};

		self.authenticated = function() {
			return auth.authenticated;
		}

		self.login = function() {
			auth.authenticate(self.credentials, function(authenticated) {
				if (authenticated) {
					console.log("Login succeeded")
					self.error = false;
				} else {
					console.log("Login failed")
					self.error = true;
				}
			})
		};

		self.logout = auth.clear;

	}
	
});
