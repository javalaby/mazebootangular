angular
		.module('hello', [ 'ui.router', 'auth','ui.bootstrap', 'home', 'message', 'navigation' ])
		.config(

				function($stateProvider, $httpProvider, $locationProvider) {

					$locationProvider.html5Mode(true);
					
					// An array of state definitions
					var states = [
						{ 
							name: 'home',
							url: '/',
							component: 'home' 
						},
						
						{ 
						  name: 'message', 
						  url: '/message', 
						  component: 'message'
						},
						
						{ 
						  name: 'navigation', 
						  url: '/login', 
						  component: 'navigation'
						}
					]
					
					// Loop over the state definitions and register them
					states.forEach(function(state) {
					  $stateProvider.state(state);
					});
					
					$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
		})
		.run(function(auth, ng1UIRouter) {

			// Initialize auth module with the home page and login/logout path
			// respectively
			auth.init('/', '/login', '/logout');
			window['ui-router-visualizer'].visualizer(ng1UIRouter);

		});
