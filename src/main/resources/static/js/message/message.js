angular.module('message', []).component('message', {
	templateUrl: 'js/message/message.html',
	
	controller : function($http) {
		var self = this;
		$http.get('/resource/').then(function(response) {
			self.greeting = response.data;
		});
	}
});
