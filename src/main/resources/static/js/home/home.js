angular.module('home', []).component('home',{
	templateUrl: 'js/home/home.html',
	
	controller : function($http) {
		var self = this;
		$http.get('/user/').then(function(response) {
			self.user = response.data.name;
		});
	}
});
