-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema maze
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `maze` ;

-- -----------------------------------------------------
-- Schema maze
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `maze` DEFAULT CHARACTER SET utf8 ;
USE `maze` ;

-- -----------------------------------------------------
-- Table `maze`.`room`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `maze`.`room` ;

CREATE TABLE IF NOT EXISTS `maze`.`room` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `type` INT(11) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `sizex` INT(11) NOT NULL,
  `sizey` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `name_UNIQUE` ON `maze`.`room` (`name` ASC);


-- -----------------------------------------------------
-- Table `maze`.`type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `maze`.`type` ;

CREATE TABLE IF NOT EXISTS `maze`.`type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `name_UNIQUE` ON `maze`.`type` (`name` ASC);


-- -----------------------------------------------------
-- Table `maze`.`design`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `maze`.`design` ;

CREATE TABLE IF NOT EXISTS `maze`.`design` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `imagepath` VARCHAR(45) NOT NULL,
  `type_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_design_type1`
    FOREIGN KEY (`type_id`)
    REFERENCES `maze`.`type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `name_UNIQUE` ON `maze`.`design` (`name` ASC);

CREATE INDEX `fk_design_type1_idx` ON `maze`.`design` (`type_id` ASC);


-- -----------------------------------------------------
-- Table `maze`.`tile`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `maze`.`tile` ;

CREATE TABLE IF NOT EXISTS `maze`.`tile` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `room_id` INT(11) NOT NULL,
  `posx` INT(11) NOT NULL,
  `posy` INT(11) NOT NULL,
  `design_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tile_room1`
    FOREIGN KEY (`room_id`)
    REFERENCES `maze`.`room` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tile_design1`
    FOREIGN KEY (`design_id`)
    REFERENCES `maze`.`design` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `unique_position` ON `maze`.`tile` (`posx` ASC, `posy` ASC, `id` ASC);

CREATE INDEX `fk_tile_room1_idx` ON `maze`.`tile` (`room_id` ASC);

CREATE INDEX `fk_tile_design1_idx` ON `maze`.`tile` (`design_id` ASC);


-- -----------------------------------------------------
-- Table `maze`.`player`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `maze`.`player` ;

CREATE TABLE IF NOT EXISTS `maze`.`player` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `maze`.`partie`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `maze`.`partie` ;

CREATE TABLE IF NOT EXISTS `maze`.`partie` (
  `id` INT NOT NULL,
  `sizex` INT NOT NULL,
  `sizey` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `maze`.`action`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `maze`.`action` ;

CREATE TABLE IF NOT EXISTS `maze`.`action` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `numero` INT NOT NULL,
  `tour` INT NOT NULL,
  `player_id` INT NOT NULL,
  `partie_id` INT NOT NULL,
  `type` INT NOT NULL,
  `arguments` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_action_player1`
    FOREIGN KEY (`player_id`)
    REFERENCES `maze`.`player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_action_partie1`
    FOREIGN KEY (`partie_id`)
    REFERENCES `maze`.`partie` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_action_player1_idx` ON `maze`.`action` (`player_id` ASC);

CREATE INDEX `fk_action_partie1_idx` ON `maze`.`action` (`partie_id` ASC);


-- -----------------------------------------------------
-- Table `maze`.`tile_has_design`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `maze`.`tile_has_design` ;

CREATE TABLE IF NOT EXISTS `maze`.`tile_has_design` (
  `tile_id` INT(11) NOT NULL,
  `design_id` INT(11) NOT NULL,
  PRIMARY KEY (`tile_id`, `design_id`),
  CONSTRAINT `fk_tile_has_design_tile1`
    FOREIGN KEY (`tile_id`)
    REFERENCES `maze`.`tile` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tile_has_design_design1`
    FOREIGN KEY (`design_id`)
    REFERENCES `maze`.`design` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE INDEX `fk_tile_has_design_design1_idx` ON `maze`.`tile_has_design` (`design_id` ASC);

CREATE INDEX `fk_tile_has_design_tile1_idx` ON `maze`.`tile_has_design` (`tile_id` ASC);


-- -----------------------------------------------------
-- Table `maze`.`room_has_partie`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `maze`.`room_has_partie` ;

CREATE TABLE IF NOT EXISTS `maze`.`room_has_partie` (
  `room_id` INT(11) NOT NULL,
  `partie_id` INT NOT NULL,
  `position` INT NOT NULL,
  PRIMARY KEY (`room_id`, `partie_id`),
  CONSTRAINT `fk_room_has_partie_room1`
    FOREIGN KEY (`room_id`)
    REFERENCES `maze`.`room` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_room_has_partie_partie1`
    FOREIGN KEY (`partie_id`)
    REFERENCES `maze`.`partie` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_room_has_partie_partie1_idx` ON `maze`.`room_has_partie` (`partie_id` ASC);

CREATE INDEX `fk_room_has_partie_room1_idx` ON `maze`.`room_has_partie` (`room_id` ASC);


-- -----------------------------------------------------
-- Table `maze`.`item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `maze`.`item` ;

CREATE TABLE IF NOT EXISTS `maze`.`item` (
  `id` INT NOT NULL,
  `design_id` INT(11) NOT NULL,
  `tile_id` INT(11) NULL,
  `player_id` INT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_item_tile1`
    FOREIGN KEY (`tile_id`)
    REFERENCES `maze`.`tile` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_design1`
    FOREIGN KEY (`design_id`)
    REFERENCES `maze`.`design` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_player1`
    FOREIGN KEY (`player_id`)
    REFERENCES `maze`.`player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_item_tile1_idx` ON `maze`.`item` (`tile_id` ASC);

CREATE INDEX `fk_item_design1_idx` ON `maze`.`item` (`design_id` ASC);

CREATE INDEX `fk_item_player1_idx` ON `maze`.`item` (`player_id` ASC);


-- -----------------------------------------------------
-- Table `maze`.`player_has_partie`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `maze`.`player_has_partie` ;

CREATE TABLE IF NOT EXISTS `maze`.`player_has_partie` (
  `player_id` INT NOT NULL,
  `partie_id` INT NOT NULL,
  `tile_id` INT(11) NOT NULL,
  PRIMARY KEY (`player_id`, `partie_id`),
  CONSTRAINT `fk_player_has_partie_player1`
    FOREIGN KEY (`player_id`)
    REFERENCES `maze`.`player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_player_has_partie_partie1`
    FOREIGN KEY (`partie_id`)
    REFERENCES `maze`.`partie` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_player_has_partie_tile1`
    FOREIGN KEY (`tile_id`)
    REFERENCES `maze`.`tile` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_player_has_partie_partie1_idx` ON `maze`.`player_has_partie` (`partie_id` ASC);

CREATE INDEX `fk_player_has_partie_player1_idx` ON `maze`.`player_has_partie` (`player_id` ASC);

CREATE INDEX `fk_player_has_partie_tile1_idx` ON `maze`.`player_has_partie` (`tile_id` ASC);


-- -----------------------------------------------------
-- Table `maze`.`partie_has_player`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `maze`.`partie_has_player` ;

CREATE TABLE IF NOT EXISTS `maze`.`partie_has_player` (
  `partie_id` INT NOT NULL,
  `player_id` INT NOT NULL,
  `tile_id` INT(11) NOT NULL,
  PRIMARY KEY (`partie_id`, `player_id`, `tile_id`),
  CONSTRAINT `fk_partie_has_player_partie1`
    FOREIGN KEY (`partie_id`)
    REFERENCES `maze`.`partie` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_partie_has_player_player1`
    FOREIGN KEY (`player_id`)
    REFERENCES `maze`.`player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_partie_has_player_tile1`
    FOREIGN KEY (`tile_id`)
    REFERENCES `maze`.`tile` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_partie_has_player_player1_idx` ON `maze`.`partie_has_player` (`player_id` ASC);

CREATE INDEX `fk_partie_has_player_partie1_idx` ON `maze`.`partie_has_player` (`partie_id` ASC);

CREATE INDEX `fk_partie_has_player_tile1_idx` ON `maze`.`partie_has_player` (`tile_id` ASC);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
