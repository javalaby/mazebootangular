Serveur rest :

/editor/api
GET	
[
	{"name":"Type", "link":"/type"},
	{"name":"Categories", "link":"/category"},
	{"name":"Scenarii", "link":"/frame"},
	{"name":"Templates", "link":"/template"},
	{"name":"Scenarii", "link":"/scenario"},
	{"name":"Map generator", "link":"/generator"}
]

/editor/api/type
GET 
[
	{"id":1, "name":"item", "timestamp":"2016-09-14 09:46:29.937022", "lastuser":"Gutz"
	},
	{"id":2, "name":"room", "timestamp":"2016-09-14 09:46:29.937022", "lastuser":"Gutz"
	}
]
PUT 
{"name":"deo"
}
PATCH
{"id":3, "name":"deco"
}
	
	
/editor/api/category
GET
[
	{"id":1, "name":"weapon/2h/sword", "timestamp":"2016-09-14 09:46:29.937022", "lastuser":"Gutz"
	},
	{"id":2, "name":"s1/t1/s1", "timestamp":"2016-09-14 09:46:29.937022", "lastuser":"Gutz"
	}
]
PUT
{"name":"meuble/chaise"
}
PATCH
{"id":3, "name":"meuble/chaise/gothic"
}

/editor/api/frame
GET 
[
	{"id":1, "name":"sword1", "width":2, "height":2, "geodata":"[0,0],[0,0]", "timestamp":"2016-09-14 09:46:29.937022", "lastuser":"Gutz"
	},
	{"id":2, "name":"ville1", "width":3, "height":3, "geodata":"[0,-1,0],[-1,-1,-1],[0,-1,0]", "timestamp":"2016-09-14 09:46:29.937022", "lastuser":"Gutz"
	}
]
PATCH
{"id":1, "width":3, "geodata":"[0,0,0],[0,-1,0]"
}

editor/api/template
GET 
[
	{"id":1, "name":"sword1", "height":1, "width":1, "timestamp":"2016-09-14 09:46:29.937022", "lastuser":"Gutz", "type_id":1, "category_id":1, "frame_id":1, "templates":null
	},
	{"id":2, "name":"salle1", "height":10, "width":10, "timestamp":"2016-09-14 09:46:29.937022", "lastuser":"Gutz", "type_id":2, "category_id":2, "frame_id":2, "templates":[{"id":1, "x":1, "y":1, "layer":0}, {"id":1, "x":5, "y":1, "layer":0}]
	}
]
PUT
{"name":"chaise1", "height":2, "width":3, "type_id":3, "category_id":3, "frame_id":1, "templates":null
}
PATCH
{"id":2, "height":6, "templates":[{"id":1, "y":2}, {"id":3, "x":5, "y":1, "layer":0.5}]
}





