package fr.aston.yatoj.maze.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import fr.aston.yatoj.maze.model.TileMark;
@Mapper
public interface TileMarkMapper {
	List<TileMark> joinTileMarkByRoomId(int roomId);
}
