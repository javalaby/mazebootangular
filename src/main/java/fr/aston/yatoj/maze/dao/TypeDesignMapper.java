package fr.aston.yatoj.maze.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import fr.aston.yatoj.maze.model.TypeDesign;
@Mapper
public interface TypeDesignMapper {
	List<TypeDesign> joinTypesDesigns();
}
