package fr.aston.yatoj.maze.dao;

import fr.aston.yatoj.maze.model.Room;
import fr.aston.yatoj.maze.model.RoomExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface RoomMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table room
	 * @mbggenerated  Tue Aug 02 16:25:41 CEST 2016
	 */
	int countByExample(RoomExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table room
	 * @mbggenerated  Tue Aug 02 16:25:41 CEST 2016
	 */
	int deleteByExample(RoomExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table room
	 * @mbggenerated  Tue Aug 02 16:25:41 CEST 2016
	 */
	int deleteByPrimaryKey(Integer id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table room
	 * @mbggenerated  Tue Aug 02 16:25:41 CEST 2016
	 */
	int insert(Room record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table room
	 * @mbggenerated  Tue Aug 02 16:25:41 CEST 2016
	 */
	int insertSelective(Room record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table room
	 * @mbggenerated  Tue Aug 02 16:25:41 CEST 2016
	 */
	List<Room> selectByExample(RoomExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table room
	 * @mbggenerated  Tue Aug 02 16:25:41 CEST 2016
	 */
	Room selectByPrimaryKey(Integer id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table room
	 * @mbggenerated  Tue Aug 02 16:25:41 CEST 2016
	 */
	int updateByExampleSelective(@Param("record") Room record, @Param("example") RoomExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table room
	 * @mbggenerated  Tue Aug 02 16:25:41 CEST 2016
	 */
	int updateByExample(@Param("record") Room record, @Param("example") RoomExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table room
	 * @mbggenerated  Tue Aug 02 16:25:41 CEST 2016
	 */
	int updateByPrimaryKeySelective(Room record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table room
	 * @mbggenerated  Tue Aug 02 16:25:41 CEST 2016
	 */
	int updateByPrimaryKey(Room record);
}