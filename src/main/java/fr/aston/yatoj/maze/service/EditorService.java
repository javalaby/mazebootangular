package fr.aston.yatoj.maze.service;

import java.util.List;

import org.springframework.dao.DataAccessException;

import fr.aston.yatoj.maze.model.Room;
import fr.aston.yatoj.maze.model.RoomJson;
import fr.aston.yatoj.maze.model.TypeDesign;
import fr.aston.yatoj.maze.model.TileMark;

public interface EditorService {
	List<Room> findRooms() throws DataAccessException;
	/*
	 * ramene la liste de toutes les rooms sans condition
	 */
	
	List<TypeDesign> findTypesDesigns() throws DataAccessException;
	/*
	 * ramene la liste de tout les types ainsi que de tout les designs associés sans condition
	 */
	
	List<TileMark> listTilesMarksByRoomId(Integer roomId) throws DataAccessException;
	/*
	 * ramene la liste de toutes les tiles avec leur marks (null s'ils n'en ont pas) pour une room donnée (triée par posx posy)
	 */

	Room findRoomById(Integer roomId) throws DataAccessException;
	/*
	 * ramene les informations d'une room donnée
	 */
	
	void saveRoom(Room room) throws DataAccessException;
	/*
	 * sauvegarde/update une room
	 */
	
	void saveRoomJson(RoomJson roomJson) throws DataAccessException;
	/*
	 * transforme une roomJson en room
	 */

	RoomJson roomToRoomJson(Room room, List<TileMark> tilesMarks) throws DataAccessException;
	/*
	 * transforme une room en une roomJson
	 */

	





}
