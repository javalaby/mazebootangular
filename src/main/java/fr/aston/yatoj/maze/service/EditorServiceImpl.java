package fr.aston.yatoj.maze.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.aston.yatoj.maze.dao.RoomMapper;
import fr.aston.yatoj.maze.dao.TileHasDesignMapper;
import fr.aston.yatoj.maze.dao.TileMapper;
import fr.aston.yatoj.maze.dao.TileMarkMapper;
import fr.aston.yatoj.maze.dao.TypeDesignMapper;
import fr.aston.yatoj.maze.model.MarkJson;
import fr.aston.yatoj.maze.model.Room;
import fr.aston.yatoj.maze.model.RoomJson;
import fr.aston.yatoj.maze.model.Tile;
import fr.aston.yatoj.maze.model.TileExample;
import fr.aston.yatoj.maze.model.TileHasDesignKey;
import fr.aston.yatoj.maze.model.TileMark;
import fr.aston.yatoj.maze.model.TypeDesign;

@Service
public class EditorServiceImpl implements EditorService {
	private static final Logger LOG = LoggerFactory.getLogger(EditorServiceImpl.class);
	
	private RoomMapper roomMapper;
	private TypeDesignMapper typeDesignMapper;
	private TileMarkMapper tileMarkMapper;
	private TileMapper tileMapper;
	private TileHasDesignMapper tileHasDesignMapper;
	
	@Autowired
	public EditorServiceImpl(RoomMapper roomMapper, TypeDesignMapper typeDesignMapper, TileMarkMapper tileMarkMapper,TileMapper tileMapper, TileHasDesignMapper tileHasDesignMapper) {
		this.roomMapper = roomMapper;
		this.typeDesignMapper = typeDesignMapper;
		this.tileMarkMapper = tileMarkMapper;
		this.tileMapper = tileMapper;
		this.tileHasDesignMapper = tileHasDesignMapper;
	}
	
	@Override
	public List<Room> findRooms() throws DataAccessException {
	/*
	 * service renvoyant la liste non triée de toutes les Rooms à afficher. Nous utilisons les fonctions dites complexes(Example) 
	 * générées par mybatis pour faire une requete sans clause where sur la table Room.
	 * dao : fr.aston.yatoh.dao.RoomMapper (src/main/java et src/main/ressource)
	 * model : fr.aston.yatoj.maze.model.Room fr.aston.yatoj.maze.model.RoomExample
	 * 
	 */
		if (LOG.isDebugEnabled()) LOG.debug("findRooms");
		return roomMapper.selectByExample(null);
	}

	@Override
	public List<TypeDesign> findTypesDesigns() throws DataAccessException {
		/*
		 * Service renvoyant la liste triée de toutes les images par type. C'est une jointure personalisée entre les tables type et design
		 * sans prédicat.
		 * dao : fr.aston.yatoj.dao.TypeDesignMapper (src/main/java et src/main/ressource)
		 * model : fr.aston.yatoj.maze.model.TypeDesign
		 */
		if (LOG.isDebugEnabled()) LOG.debug("findTypesDesigns");
		return typeDesignMapper.joinTypesDesigns();
	}
	
	@Override
	public List<TileMark> listTilesMarksByRoomId(Integer roomId) throws DataAccessException {
		/*
		 * Service renvoyant la liste non triée de toutes les Marks pour une room passé en parametre. Une mark est en fait l'association
		 * tile_has_design, nous l'utiliserons pour par exemple pour modélisé les differentes portes de sorties possible pour une
		 * room ou encore les differents coffres du meme type dans une salle.
		 * C'est une jointure personalisée entre les tables Tile et tile_has_design par roomId
		 * dao : fr.aston.yatoj.dao.TileMarkMapper (src/main/java et src/main/ressource)
		 * model : fr.aston.yatoj.maze.model.TileMark
		 */
		if (LOG.isDebugEnabled()) LOG.debug("listTilesByRoomId roomId : {}", roomId);
		return tileMarkMapper.joinTileMarkByRoomId(roomId);
	}
	
	@Override
	public Room findRoomById(Integer roomId) throws DataAccessException {
		/*
		 * service renvoyant les informations d'une room dont l'id a été passé en parametre.
		 * Nous utilisons les fonctions dites simples générées par mybatis (CRUD)
		 * dao : fr.aston.yatoj.dao.RoomMapper (src/main/java et src/main/ressource)
		 * model : fr.aston.yatoj.maze.model.Room
		 */
		if (LOG.isDebugEnabled()) LOG.debug("findRoomById id :{}", roomId);
		return roomMapper.selectByPrimaryKey(roomId);
	}
		
	@Override
	public void saveRoom(Room room) throws DataAccessException {
		/*
		 * service inserant une room passé en paramètre. si l'idRoom est renseigné fait une maj et supprime les tiles à la place.
		 * Nous utilisons les fonctions dites simples générées par mybatis (CRUD)
		 * dao : fr.aston.yatoj.dao.RoomMapper (src/main/java et src/main/ressource)
		 * model : fr.aston.yatoj.maze.model.Room
		 */
		if (LOG.isDebugEnabled()) LOG.debug("saveRoom id :{}", room.getId());
		if (room.getId() == null) {
			roomMapper.insert(room);
		}
		else {
			roomMapper.updateByPrimaryKeySelective(room);
			deleteTileByRoomId(room.getId());
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveRoomJson(RoomJson roomJson) {
	/*
	 * Service sauvegardant une Room et ses infos associées envoyées par le front.
	 */
		if (LOG.isDebugEnabled()) LOG.debug("saveTile id :{}", roomJson.getId(), roomJson.getName());
		
		Integer roomJsonId = roomJson.getId();
		
    	Room room = new Room();
    	
    	room.setId(roomJsonId);
    	room.setName(roomJson.getName());
    	room.setSizex(roomJson.getSizex());
    	room.setSizey(roomJson.getSizey());
    	room.setType(roomJson.getType());

    	/*
    	 * si l'idRoom est renseigné update la room et supprime toute les tiles
    	 */
    	saveRoom(room);
    	
    	int roomId = room.getId();
    	
        int posX = 1;
        int posY = 1;
    	
        //idDesignTiles est un tableau 2d de idDesign envoyé par le front. la position dans le tableau 1 represent x et dans le 2 y
        for (List<Integer> idDesigns: roomJson.getIdDesignTiles()) {
        	posY = 1;
        	if (idDesigns != null) {
        		for (Integer idDesign : idDesigns) {
        			if (idDesign != null) {
        				Tile tile = new Tile();
        				tile.setRoomId(roomId);
        				tile.setPosx(posX);
        				tile.setPosy(posY);
        				tile.setDesignId(idDesign);
        				
        				tileMapper.insert(tile);
        				
        				posY++;
        			}
        		}
        		posX++;
        	}	
		}
        
        for (MarkJson markJson : roomJson.getIdDesignMarks()) {
        /*
         * Nous recherchons l'id de la tile par sa position room, x, y
         */
        	TileHasDesignKey mark = new TileHasDesignKey();
        	mark.setDesignId(markJson.getIdDesign());
        	mark.setTileId(findIdTileByPos(roomId, markJson.getPosX(), markJson.getPosY()));
        	tileHasDesignMapper.insert(mark);
		}
        
	}
	
	public void deleteTileByRoomId(Integer id) {
	/*
	 * service faisant un delete de toutes les tiles ayant la roomId passé en paramètre.
	 * nous utilisons les fonctions générées par MyBatis dites by example
	 *  dao : fr.aston.yatoj.dao.TileMapper (src/main/java et src/main/ressource)
	 *  model : fr.aston.yatoj.maze.model.Tile fr.aston.yatoj.maze.TileExample
	 */
		TileExample example = new TileExample();
		example.or().andRoomIdEqualTo(id);
		tileMapper.deleteByExample(example);
	}

	public Integer findIdTileByPos(int roomId, Integer posX, Integer posY) {
	/*
	 * service recuperant l'idTile d'une tile par sa position room,x,y.
	 * nous utilisons les fonctions générées par MyBatis dites by example
	 *  dao : fr.aston.yatoj.dao.TileMapper (src/main/java et src/main/ressource)
	 *  model : fr.aston.yatoj.maze.model.Tile fr.aston.yatoj.maze.TileExample
	 */
		TileExample example = new TileExample();
		example.or()
			.andRoomIdEqualTo(roomId)
			.andPosxEqualTo(posX)
			.andPosyEqualTo(posY);
		
		return tileMapper.selectByExample(example).get(0).getId();
	}

	@Override
	public RoomJson roomToRoomJson(Room room, List<TileMark> tilesMarks) throws DataAccessException {
		if (LOG.isDebugEnabled()) LOG.debug("roomToRoomJson idRoom : {}, sizeTiles : {}", room.getId(), tilesMarks.size());
        
    	RoomJson roomJson = new RoomJson();
    	
    	Integer idRoom = room.getId();
    	Integer sizeY = room.getSizey();
    	
    	roomJson.setId(idRoom);
    	
    	if (LOG.isDebugEnabled()) LOG.debug("room.getName() : {}", room.getName() ); 
    	roomJson.setName(room.getName());
    	
    	if (LOG.isDebugEnabled()) LOG.debug("room.getSizex() : {}", room.getSizex()); 
    	roomJson.setSizex(room.getSizex());
    	
    	if (LOG.isDebugEnabled()) LOG.debug("sizeY : {}", sizeY); 
    	roomJson.setSizey(sizeY);
    	
    	if (LOG.isDebugEnabled()) LOG.debug("room.getType() : {}", room.getType()); 
    	roomJson.setType(room.getType());
    	
    	
    	//if (LOG.isDebugEnabled()) LOG.debug("roomJson.getIdDesignTiles() : {}", roomJson.getIdDesignTiles()); 		
        roomJson.getIdDesignTiles().add(null);
        
        List<Integer> listY = new ArrayList<Integer>();
        //la première position est null pour les besoin du front. (0,0)(0,y)(x,0) n'existent pas
        listY.add(null);

        //idDesignTiles est un tableau 2d de idDesign à envoyé au front. la position dans le tableau 1 represent x et dans le 2 y
        for (TileMark tileMark : tilesMarks) {
        	listY.add(tileMark.getDesignId());
        	
        	if (tileMark.getMarks() != null){
        		for (Integer design : tileMark.getMarks()) {
					roomJson.getIdDesignMarks().add(new MarkJson(tileMark.getPosx(), tileMark.getPosy(), design));
				}
        	}
        	
            if (tileMark.getPosy() == sizeY) {
            	roomJson.getIdDesignTiles().add(listY);
                listY = new ArrayList<Integer>();
                listY.add(null);
            }
        }
        
        return roomJson ;
        
    }
}
