package fr.aston.yatoj.maze.web;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.aston.yatoj.maze.model.Room;
import fr.aston.yatoj.maze.model.RoomJson;
import fr.aston.yatoj.maze.model.TileMark;
import fr.aston.yatoj.maze.service.EditorService;

@Controller
public class EditorController {
	/*
	 * Controller de la partie editeur du site(HTML).
	 * Seul un admin aura accès à ces mappings
	 */
	private final EditorService editorService;
	private static final Logger LOG = LoggerFactory.getLogger(EditorController.class);	
	
    @Autowired
    public EditorController(EditorService editorService) {
        this.editorService = editorService;
    }
	
    @RequestMapping(value = "/admin/editor/", method = RequestMethod.GET)
    public String initNewEditorRoomJson(Map<String, Object> model) {
    	if (LOG.isDebugEnabled()) LOG.debug("RequestMapping /admin/editor/ GET");
    	model.put("roomJson", new RoomJson()); 
    	return "/admin_editor";
	}
    
    @RequestMapping(value = "/admin/{roomId}/editor/", method = RequestMethod.GET)
    public String initExistingEditorRoomJson(@PathVariable("roomId") int roomId,Map<String, Object> model) {
    	if (LOG.isDebugEnabled()) LOG.debug("RequestMapping /admin/editor/ GET");
    	Room room = editorService.findRoomById(roomId);
    	List<TileMark> tilesMarks = editorService.listTilesMarksByRoomId(roomId);
    	RoomJson roomJson = editorService.roomToRoomJson(room,tilesMarks);
    	model.put("roomJson", roomJson);
    	return "/admin_editor";
	}
    
    @RequestMapping(value = "/admin/editor/", method = RequestMethod.POST)
    public String processEditorRoomJson(@Valid RoomJson roomJson, BindingResult result, @PathVariable("roomId") int roomId) {
    	if (LOG.isDebugEnabled()) LOG.debug("RequestMapping /admin/editor/ GET");
    	if (result.hasErrors()) {
    		return "/admin_editor";
    	}
    	else {
    		this.editorService.saveRoomJson(roomJson);
    		roomId = roomJson.getId();
    		/*
    		 * a test redirection vers roomId
    		 */
    		return "redirect:/admin/{roomId}/editor/"; 
    	}
	}

}
