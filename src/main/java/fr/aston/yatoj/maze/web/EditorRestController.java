package fr.aston.yatoj.maze.web;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.aston.yatoj.maze.model.Room;
import fr.aston.yatoj.maze.model.RoomJson;
import fr.aston.yatoj.maze.model.TileMark;
import fr.aston.yatoj.maze.model.TypeDesign;
import fr.aston.yatoj.maze.service.EditorService;

@RestController
@RequestMapping("/editor")
public class EditorRestController {
	/*
	 * Controller de la partie editeur du site.
	 * Seul un admin aura accès à ces mappings
	 */
	private final EditorService editorService;
	private static final Logger LOG = LoggerFactory.getLogger(EditorRestController.class);
		
    @Autowired
    public EditorRestController(EditorService editorService) {
        this.editorService = editorService;
    }
    
    @RequestMapping(value = "/designs", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TypeDesign>> listAllDesigns() {
    	/*
    	 * Renvoie un objet Json contenant une liste de type et leurs designs disponibles.
    	 * Pour l'instant un design a un id, un type, un name et un imagepath
    	 * Pour l'instant un type a un id et un name. Il serait bien de prevoir des sous-types.
    	 * 
    	 * Structure de la réponse Json :
    	 * [nameType,
    	 * designs [{idDesign,
    	 * 			nameDesign,
    	 * 			imagepathDesign}]
    	 * ]  
    	 * 
    	 * si on trouve des resultats on renvoie un objet json et le statut 200 OK dans le header
    	 * si on ne trouve pas de resultats on ne renvoie rien et le statut 404 NO_CONTENT dans le header
    	 * si il y a une erreur on ne renvoie rien et le statut 406 INNACEPTABLE(?) dans le header
    	 */
    	if (LOG.isDebugEnabled()) LOG.debug("RequestMapping /editor/designs/ GET");
    	List<TypeDesign> typesDesigns = editorService.findTypesDesigns();
              
        if(typesDesigns == null){
            return new ResponseEntity<List<TypeDesign>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }        
        return  new ResponseEntity<List<TypeDesign>>(typesDesigns, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/rooms", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Room>> getRoomsJson() {
    	/*
    	 * renvoie un objet Json contenant une liste de Room contenant toutes les rooms qui sont disponibles dans la base.
    	 * Pour l'instant une room a un id un type un name une tailleX une tailleY
    	 * 
    	 * structure de la réponse Json
    	 * [{idRoom,
    	 * 	typeRoom,
    	 * 	nameRoom,
    	 * 	sizex,
    	 * 	sizey
    	 * }]
    	 * 
    	 * si on trouve des resultats on renvoie un objet json et le statut 200 OK dans le header
    	 * si on ne trouve pas de resultats on ne renvoie rien et le statut 404 NO_CONTENT dans le header
    	 * si il y a une erreur on ne renvoie rien et le statut 406 INNACEPTABLE(?) dans le header
    	 */
    	if (LOG.isDebugEnabled()) LOG.debug("RequestMapping /editor/room/{}/ GET ");

    	List<Room> rooms = editorService.findRooms();
    		
    	if (rooms.isEmpty()) {	
    		return new ResponseEntity<List<Room>>(HttpStatus.NOT_FOUND);
    	}
    	return new ResponseEntity<List<Room>>(rooms, HttpStatus.OK);
    	
    }   
    
    @RequestMapping(value = "/room/{roomId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RoomJson> getRoomJson(@PathVariable("roomId") Integer roomId) {
    	/*
    	 * renvoie un objet Json RoomJson contenant toute les informations dont le front a besoin pour afficher une room 
    	 * dans l'editeur.
    	 * Pour l'instant une room a un id un type un name une tailleX une tailleY.
    	 * Elle a aussi une liste de (tailleX * tailleY) Tile associées.
    	 * Une tile a une position (X, Y) et un design.
    	 * Elle peut en plus de son design avoir des designs associés que nous appelerons markers.
    	 * Nous utiliserons ces associations designs/tile pour materialisé des entrées possibles ou encore des endroits
    	 * ou des objets peuvent se trouver.
    	 * 
    	 * Pour les besoins du front et dans un souci d'optimisation la reponse est un objet Json personalisé composé de :
    	 * {idRoom,
    	 * typeRoom,
    	 * nameRoom,
    	 * tailleXRoom,
    	 * tailleYRoom,
    	 * *(tableau a 2d, la position dans le premier tableau represente la position x, la position dans le deuxieme la position y)
    	 * listListIdDesign [[idDesign]], 
    	 * listMarkers [x, y, idDesign]
    	 * }
    	 * 
    	 * renvoie 
    	 */
    	if (LOG.isDebugEnabled()) LOG.debug("RequestMapping /editor/room/{}/ GET ", roomId);
    	try {
    		Room room = editorService.findRoomById(roomId);
    		
        	if (room == null) {	
        		return new ResponseEntity<RoomJson>(HttpStatus.NOT_FOUND);
        	}
        		
        	List<TileMark> tilesMarks = editorService.listTilesMarksByRoomId(roomId);
        	
        	if (tilesMarks.isEmpty()) {	
        		return new ResponseEntity<RoomJson>(HttpStatus.NOT_FOUND);
        	}        	
        	
        	RoomJson roomJson = editorService.roomToRoomJson(room,tilesMarks);

    		return new ResponseEntity<RoomJson>(roomJson, HttpStatus.OK);
    	}
    	catch (EmptyResultDataAccessException nre){
    		if (LOG.isDebugEnabled()) LOG.warn("room id :{} not found", roomId);
    		return new ResponseEntity<RoomJson>(HttpStatus.NOT_FOUND);//You many decide to return HttpStatus.NOT_FOUND
    	}
    }
    
    @RequestMapping(value = "/room/tiles", method = RequestMethod.POST)
    public ResponseEntity<Void> createRoomTilesJson(@RequestBody RoomJson roomJson){
    	/*
    	 * Recupère un objet Json composé de
    	 * {idRoom,
    	 * typeRoom,
    	 * nameRoom,
    	 * tailleXRoom,
    	 * tailleYRoom,
    	 * listListTiles [[idDesign]] (tableau a 2d, la position dans le premier tableau represente la position x, la position dans le deuxieme la position y)
    	 * listMarkers [x, y, idDesign] 
    	 * }
    	 * 
    	 * -si l'idRoom est null on créée une nouvelle room et on insere les tiles associées avec leur markers associés.
    	 * 		on insere les tiles avec leur idDesign et leur position
    	 * 		on insere les markers (association idDesign/idTile)		
    	 * -si l'idRoom n'est pas null : 
    	 * 		on met a jour le type nam tailleX tailleY
    	 * 		on supprime les tiles associées
    	 * 		on insere les tiles avec l'idRoom transmis et leurs idDesigns
    	 * 		on insere les association markers (idDesign/idTile)
    	 * 
    	 * Pour une map de taille (5,5) il lui faut 25 tuiles, la premiere tile est id 1, 
    	 * la tile position (5,3) est id (5*4 +3 ) 23 et les markers avec l'idroom créée
    	 */
    	if (LOG.isDebugEnabled()) LOG.debug("RequestMapping /editor/room/tiles PUT");
    	
    	editorService.saveRoomJson(roomJson);
    	
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    } 
    
    

}
