package fr.aston.yatoj.maze.model;

import java.util.ArrayList;
import java.util.List;

public class RoomJson {	
	private Integer id;

	private String name;

	private Integer sizex;

	private Integer sizey;
	
	private Integer type;
	
	private List<List<Integer>> idDesignTiles;
	
	private List<MarkJson> idDesignMarks;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSizex() {
		return sizex;
	}

	public void setSizex(Integer sizex) {
		this.sizex = sizex;
	}

	public Integer getSizey() {
		return sizey;
	}

	public void setSizey(Integer sizey) {
		this.sizey = sizey;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public List<List<Integer>> getIdDesignTiles() {
		if (idDesignTiles == null) {
			idDesignTiles = new ArrayList<List<Integer>>();
		}
		return idDesignTiles;
	}

	public void setIdDesignTiles(List<List<Integer>> idDesignTiles) {
		this.idDesignTiles = idDesignTiles;
	}

	public List<MarkJson> getIdDesignMarks() {
		if (idDesignMarks == null) {
			idDesignMarks = new ArrayList<MarkJson>();
		}
		return idDesignMarks;
	}

	public void setIdDesignMarks(List<MarkJson> idDesignMarks) {
		this.idDesignMarks = idDesignMarks;
	}
	



}
