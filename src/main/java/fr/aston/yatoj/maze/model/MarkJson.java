package fr.aston.yatoj.maze.model;

public class MarkJson {
	private Integer posX;
	
	private Integer posY;
	
	private Integer idDesign;

	public Integer getPosX() {
		return posX;
	}

	public void setPosX(Integer posX) {
		this.posX = posX;
	}

	public Integer getPosY() {
		return posY;
	}

	public void setPosY(Integer posY) {
		this.posY = posY;
	}

	public Integer getIdDesign() {
		return idDesign;
	}

	public void setIdDesign(Integer idDesign) {
		this.idDesign = idDesign;
	}

	public MarkJson(Integer posX, Integer posY, Integer idDesign) {
		super();
		this.posX = posX;
		this.posY = posY;
		this.idDesign = idDesign;
	}

	public MarkJson() {
		super();
	}
	

	
	
	
	
}
