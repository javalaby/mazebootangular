package fr.aston.yatoj.maze.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class TypeDesign {
	@JsonIgnore
	private Integer id;
	private String name;
	private List<Design> designs;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Design> getDesigns() {
		return designs;
	}
	public void setDesigns(List<Design> designs) {
		this.designs = designs;
	}
	
	public TypeDesign(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public TypeDesign() {
		super();
	}
	
	
	
}
