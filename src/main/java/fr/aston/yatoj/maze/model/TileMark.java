package fr.aston.yatoj.maze.model;

import java.util.List;

public class TileMark {
	private Integer id;
	
	private Integer roomId;
	
	private Integer posx;
	
	private Integer posy;
	
	private Integer designId;
	
	private List<Integer> marks;
	
	public Integer getId() {
	    return id;
	}
	
	public void setId(Integer id) {
	    this.id = id;
	}
	
	public Integer getRoomId() {
	    return roomId;
	}
	
	public void setRoomId(Integer roomId) {
	    this.roomId = roomId;
	}
	
	public Integer getPosx() {
	    return posx;
	}
	
	public void setPosx(Integer posx) {
	    this.posx = posx;
	}
	
	public Integer getPosy() {
	    return posy;
	}
	
	public void setPosy(Integer posy) {
	    this.posy = posy;
	}
	
	public Integer getDesignId() {
	    return designId;
	}
	
	public void setDesignId(Integer designId) {
	    this.designId = designId;
	}

	public List<Integer> getMarks() {
		return marks;
	}

	public void setMarks(List<Integer> marks) {
		this.marks = marks;
	}
}
